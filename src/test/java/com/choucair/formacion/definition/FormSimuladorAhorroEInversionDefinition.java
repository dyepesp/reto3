package com.choucair.formacion.definition;

import java.util.List;

import com.choucair.formacion.steps.FormSimuladorAhorroEInversionSteps;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import net.thucydides.core.annotations.Steps;

public class FormSimuladorAhorroEInversionDefinition {

	@Steps
	FormSimuladorAhorroEInversionSteps formSimuladorAhorroEInversionSteps;
	
	@Given("^llena el formulario del simulador de ahorro e inversion$")
	public void llena_el_formulario_del_simulador_de_ahorro_e_inversion(DataTable dbtable) throws Throwable {
		List<List<String>> data = dbtable.raw();
		formSimuladorAhorroEInversionSteps.irALlenarElFormulario(data);
	}
	
	@Given("^ir a Disposiciones legales$")
	public void ir_a_Disposiciones_legales() throws Throwable {
		formSimuladorAhorroEInversionSteps.irADisposicionesLegales();
	}
	
	@Given("^ir a Plan de ahorro y/o inversion$")
	public void ir_a_Plan_de_ahorro_y_o_inversion() throws Throwable {
		formSimuladorAhorroEInversionSteps.irAPlanDeAhorro();
	}
}
