package com.choucair.formacion.definition;

import com.choucair.formacion.steps.NecesidadesSteps;

import cucumber.api.java.en.Given;
import net.thucydides.core.annotations.Steps;

public class NecesidadesDefinition {

	@Steps
	NecesidadesSteps necesidadesSteps;
	
	@Given("^darle click a Estudio$")
	public void darle_click_a_Estudio() throws Throwable {
		necesidadesSteps.irAEstudio();
	}

}
