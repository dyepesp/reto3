package com.choucair.formacion.definition;

import com.choucair.formacion.steps.EstudioSteps;

import cucumber.api.java.en.Given;
import net.thucydides.core.annotations.Steps;

public class EstudioDefinition {

	@Steps
	EstudioSteps estudioSteps;
	
	@Given("^darle click a simula tus ahorros de la cuenta ahorrador$")
	public void darle_click_a_simula_tus_ahorros_de_la_cuenta_ahorrador() throws Throwable {
		estudioSteps.irASimularAhorroInversion();
	}
}
