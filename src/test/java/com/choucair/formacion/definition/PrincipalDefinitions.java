package com.choucair.formacion.definition;

import com.choucair.formacion.steps.PrincipalSteps;

import cucumber.api.java.en.Given;
import net.thucydides.core.annotations.Steps;

public class PrincipalDefinitions {
	
	@Steps
	PrincipalSteps principalSteps;

	@Given("^ingreso a la pagina principal$")
	public void ingreso_a_la_pagina_principal() throws Throwable {
		principalSteps.ingresar();
	}
	
	@Given("^darle click a Necesidades$")
	public void darle_click_a_Necesidades() throws Throwable {
		principalSteps.irANecesidades();
	}
}
