package com.choucair.formacion.pageobjects;

import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;

public class EstudioPage extends PageObject {
	
	// Click en Necesidades
	@FindBy(xpath = "//*[@id='wizard1']/div/div[1]/div/div/div[1]/p[2]/strong/a")
	public WebElement simulaTusAhorros;
	
	public void mostrarSimuladorAhorroInversion() {
		simulaTusAhorros.click();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
