package com.choucair.formacion.pageobjects;

import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://www.grupobancolombia.com/wps/portal/personas")
public class PrincipalPage extends PageObject {
	
	// Click en Necesidades
	@FindBy(xpath = "//*[@id='main-menu']/div[2]/ul[1]/li[2]/a")
	public WebElement necesidades;
	
	public void mostrarOpcionNecesidades() {
		necesidades.click();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	

}
