package com.choucair.formacion.pageobjects;

import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;

public class NecesidadesPage extends PageObject {
	
	// Click en Necesidades
	@FindBy(xpath = "//*[@id='necesidadesPersonas']/div/div[1]/div[1]/div/div[3]/div/a")
	public WebElement estudio;
	

	public void mostrarOpcionEstudio() {
		estudio.click();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
	}

}
