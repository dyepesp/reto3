package com.choucair.formacion.pageobjects;

import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class FormSimuladorAhorroEInversionPage extends PageObject {
	
	@FindBy(name = "nmSelectAhorro")
	public WebElementFacade nmSelectAhorro;
	
	@FindBy(name = "nmMesesAhorro")
	public WebElement nmMesesAhorro;
	
	@FindBy(name = "nmselectProducto")
	public WebElementFacade nmselectProducto;
	
	@FindBy(name = "nmCantidadAhorro")
	public WebElement nmCantidadAhorro;
	
	@FindBy(name = "checkAutorizacion")
	public WebElement checkAutorizacion;
	
	@FindBy(xpath = "//*[@id='browser-Off']/div/ul/li[2]/a/div[2]/div/p")
	public WebElement secDisposicionesLegales;
	
	@FindBy(xpath = "//*[@id='browser-Off']/div/ul/li[3]/a/div[2]/div/p")
	public WebElement secPlanDeAhorro;
	
	@FindBy(xpath = "//*[@id='browser-Off']/div/ul/li[3]/a/div[2]/div/p")
	public WebElement planDeAhorroInversion;
	
	@FindBy(xpath = "//*[@id='browser-Off']/div/div/div[1]/form/div[8]/button")
	public WebElement agregarPlanDeAhorros;
	
	@FindBy(xpath = "//*[@id='tablaAhorro']/table/tbody/tr[2]/td[2]")
	public WebElement montoAAhorrar;

	@FindBy(xpath = "//*[@id='browser-Off']/div/div/div[2]/div[3]/form/button")
	public WebElement calcularAhorro;
	
	@FindBy(xpath = "//*[@id='browser-Off']/div/div/div[1]/form/div[9]/table/tbody/tr[3]/td[1]")
	public WebElement suenio;
	
	@FindBy(xpath = "//*[@id='browser-Off']/div/div/div[1]/form/div[9]/table/tbody/tr[3]/td[2]")
	public WebElement mesesPage;
	
	@FindBy(xpath = "//*[@id='browser-Off']/div/div/div[1]/form/div[9]/table/tbody/tr[3]/td[3]")
	public WebElement monto;
	
	@FindBy(xpath = "//*[@id='browser-Off']/div/div/div[1]/form/div[9]/table/tbody/tr[3]/td[4]")
	public WebElement productoSoli;
	
	public void ingresarDatosAlFormulario(String paraQue, String meses, String producto, String cantidadFalta) {
		
		
		nmSelectAhorro.click();
		nmSelectAhorro.selectByVisibleText(paraQue);
		
		nmMesesAhorro.sendKeys(meses);
		
		nmselectProducto.click();
		nmselectProducto.selectByVisibleText(producto);
		
		nmCantidadAhorro.sendKeys(cantidadFalta);
		
		agregarPlanDeAhorros.click();
		
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		int montoFinal = Integer.parseInt(monto.getText().replace(",", "").replace("$", "").replace(".", ""))/100;
		
		assertThat(suenio.getText(), is(paraQue));
		assertThat(mesesPage.getText(), is(meses));
		assertThat(String.valueOf(montoFinal), is(cantidadFalta));
		assertThat(productoSoli.getText(), is(producto));
		
	}

	public void gestionarDisposicionesLegales() {
		secDisposicionesLegales.click();
		checkAutorizacion.click();
		calcularAhorro.click();
	}

	public void gestionarPlanDeAhorro() {
		secPlanDeAhorro.click();
		System.out.println("El monto a ahorras mensualmente es: " + montoAAhorrar.getText());

		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
