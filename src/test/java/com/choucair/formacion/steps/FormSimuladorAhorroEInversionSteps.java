package com.choucair.formacion.steps;

import java.util.List;

import com.choucair.formacion.pageobjects.FormSimuladorAhorroEInversionPage;

public class FormSimuladorAhorroEInversionSteps {
	
	FormSimuladorAhorroEInversionPage formSimuladorAhorroEInversionPage;

	public void irALlenarElFormulario(List<List<String>> data) {
		String paraQue = data.get(0).get(0);
		String meses = data.get(0).get(1);
		String producto = data.get(0).get(2);
		String cantidadFalta = data.get(0).get(3);
		formSimuladorAhorroEInversionPage.ingresarDatosAlFormulario(paraQue, meses, producto, cantidadFalta);
		
	}

	public void irADisposicionesLegales() {
		formSimuladorAhorroEInversionPage.gestionarDisposicionesLegales();
	}

	public void irAPlanDeAhorro() {
		formSimuladorAhorroEInversionPage.gestionarPlanDeAhorro();
	}

}
