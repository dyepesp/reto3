package com.choucair.formacion.steps;

import com.choucair.formacion.pageobjects.PrincipalPage;

import net.thucydides.core.annotations.Step;

public class PrincipalSteps  {
	
	PrincipalPage principalPage;
	
	@Step
	public void ingresar() {
		principalPage.open();
	}

	public void irANecesidades() {
		principalPage.mostrarOpcionNecesidades();	
	}

}
