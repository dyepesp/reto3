#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Title of your feature
  I want to use this template for my feature file

  @tag1
  Scenario: Title of your scenario
    Given I want to write a step with precondition
    And some other precondition
    When I complete action
    And some other action
    And yet another action
    Then I validate the outcomes
    And check more outcomes

  @SimuladorAhorro
  Scenario Outline: Simulador ahorro 
    Given ingreso a la pagina principal 
    And darle click a Necesidades
    And darle click a Estudio
    And darle click a simula tus ahorros de la cuenta ahorrador 
    And llena el formulario del simulador de ahorro e inversion
          | <ParaQue>  | <Meses> | <Producto>  | <Dinero>	|
    And ir a Disposiciones legales 
    And ir a Plan de ahorro y/o inversion      
  #  When I check for the <value> in step
  #  Then I verify the <status> in step

    Examples: 
			| ParaQue  | Meses | Producto  | Dinero		|
			|Educación |12		 |Fiducuenta | 5000000	|